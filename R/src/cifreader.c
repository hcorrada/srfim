#include "cifreader.h"
#include "Rdefines.h"
#include <stdlib.h>

SEXP _getCifInfo(SEXP filename)
{
	SEXP ans = R_NilValue;
	SEXP snCycles = R_NilValue;
	SEXP sclusterCount = R_NilValue;

	int *nCycles, *clusterCount;

	Rprintf("Reading cif file %s\n", CHAR(STRING_ELT(filename, 0)));

	CIFData data = getCifInfo(CHAR(STRING_ELT(filename, 0)));
	if (data == NULL) {
		return ans;
	}

	PROTECT(snCycles = allocVector(INTSXP, 1));
	nCycles = INTEGER(snCycles);
	nCycles[0] = data->nCycles;

	PROTECT(sclusterCount = allocVector(INTSXP, 1));
	clusterCount = INTEGER(sclusterCount);
	clusterCount[0] = data->clusterCount;

	PROTECT(ans = allocVector(VECSXP, 2));
	SET_VECTOR_ELT(ans, 0, snCycles);
	SET_VECTOR_ELT(ans, 1, sclusterCount);

	free(data);
	UNPROTECT(3);
	return ans;
}

SEXP readcifR(SEXP filename)
{
	SEXP ans = R_NilValue;
	SEXP sintens = R_NilValue;
	SEXP snCycles = R_NilValue;
	SEXP sclusterCount = R_NilValue;

	int *nCycles, *clusterCount, *precision;
	int *intens;
	void *buffer;
	int i;

	CIFData data;
	size_t nIntens;

	data = readcif(CHAR(STRING_ELT(filename, 0)));
	if (data == NULL) {
		Rprintf("Error in readcif");
		return ans;
	}

	nIntens = getIntensSize(data);
	PROTECT(sintens = allocVector(INTSXP, nIntens));
	intens = INTEGER(sintens);
	buffer = getIntensBuffer(data);

	for (i=0; i<nIntens; i++) {
		switch(data->precision) {
			case 1:
			intens[i] = (int) ((int8_t *) buffer)[i];
			break;

			case 2:
			intens[i] = (int) ((int16_t *) buffer)[i];
			break;

			case 4:
			intens[i] = (int) ((int32_t *) buffer)[i];
			break;
		}
	}
	freeIntens(data); 

	PROTECT(snCycles = allocVector(INTSXP, 1));
	nCycles = INTEGER(snCycles);
	nCycles[0] = data->nCycles;

	PROTECT(sclusterCount = allocVector(INTSXP, 1));
	clusterCount = INTEGER(sclusterCount);
	clusterCount[0] = data->clusterCount;

	free(data);

	PROTECT(ans = allocVector(VECSXP, 3));
	SET_VECTOR_ELT(ans, 0, sintens);
	SET_VECTOR_ELT(ans, 1, snCycles);
	SET_VECTOR_ELT(ans, 2, sclusterCount);

	UNPROTECT(4);
	return ans;
}
