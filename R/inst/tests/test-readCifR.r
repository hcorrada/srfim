library(ShortRead)
datadir=file.path(getwd(),"../../../testing/RtaIntensity/Data/Intensities")

test_that("cifreader works", {
	res = readCifR(datadir, lane=6L, tiles=33L, cycles=1L)
	res2 = readIntensities(datadir, lane=6L, tiles=33L, cycles=1L, type="RtaIntensity", withVariability=FALSE)
	expect_that(res[,,1], equals(intensity(res2)[[,,,]]))
})

test_that("multiple cycles work", {
  res = readCifR(datadir, lane=6L, tiles=33L, cycles=1:3)
  res2 = readIntensities(datadir, lane=6L, tiles=33L, cycles=1:3, type="RtaIntensity", withVariability=FALSE)
  expect_that(res, equals(intensity(res2)[[,,,]]))
})