#' @export
readCifR <- function(datadir, lane, tiles, cycles) {
  filename=file.path(datadir, sprintf("L%03d", lane), 
                     sprintf("C%d.1", cycles[1]), 
                     sprintf("s_%d_%d.cif", lane, tiles[1]))

  info = .Call("_getCifInfo", filename, package="srfim");
  if (is.null(info)) 
    error("Error getting cif file info")
  
  clusterCount = info[[2]]

  dimnames = list(NULL,c("A","C","G","T"),sprintf("C%d.1", cycles))
  res = array(0L,dim=c(clusterCount, 4, length(cycles)),dimnames=dimnames)

  for (k in seq_along(cycles)) {
    filename=file.path(datadir, sprintf("L%03d", lane),
                       sprintf("C%d.1", cycles[k]),
                       sprintf("s_%d_%d.cif", lane, tiles[1]))  
	curRes = .Call("readcifR", filename, package="srfim");
    if (is.null(curRes))
      stop("Error reading data for cycle ", cycles[k])

    curClusterCount = curRes[[3]]
    if(curClusterCount != clusterCount) {
      stop("Error reading data for cycle ", cycles[k], " differeing cluster counts ", curClusterCount, clusterCount)    	
    }

    res[,,k]=curRes[[1]]
  }
  res	
}