#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <assert.h>
#include "cifreader.h"

CIFData readcif(const char *filename)
{
	CIFData data;

	data = getCifInfo(filename);
	if (data == NULL) {
		perror("Error getting info");
		return NULL;
	}

	if(allocIntens(data)) {
		perror("Error allocating buffer");
		free(data);
		return NULL;
	}

	if(readcif_data(data)) {
		perror("Error reading data");
		free(data);
		return NULL;
	}
	return data;
}

CIFData getCifInfo(const char *filename)
{
	printf("Reading cif info from file %s\n", filename);
	char magic[4] = "\0\0\0\0";
	//open file 
	FILE * pFile;

	pFile = fopen(filename, "rb");
	if (pFile == NULL) {
		perror("Error opening file");
		return NULL;
	}

	fread(magic, 1, 3, pFile);
	if(strncmp(magic, "CIF", 3)) {
		printf("Got magic string %s", magic);
		fclose(pFile);
		return NULL;
	}

	CIFData data = malloc(sizeof(struct cifData));

	fread(&(data->version), sizeof(data->version), 1, pFile);
	fread(&(data->precision), sizeof(data->precision), 1, pFile);
	fread(&(data->cycle), sizeof(data->cycle), 1, pFile);
	fread(&(data->nCycles), sizeof(data->nCycles), 1, pFile);
	fread(&(data->clusterCount), sizeof(data->clusterCount), 1, pFile);
/*
*	printf("Version: %d\n", data->version);
*	printf("Precision: %d\n",data->precision);
*	printf("cycleRead: %u\n", data->cycle);
*	printf("nCycles: %u\n", data->nCycles);
*	printf("clusterCount %u\n",data->clusterCount);
*/
	fclose(pFile);
	data->filename = filename;
	return data;
}

size_t getIntensSize(CIFData data) {
	size_t nIntensities = NCHANNELS * data->clusterCount * data->nCycles;
	return nIntensities;
}

int allocIntens(CIFData data)
{
	size_t nIntensities = getIntensSize(data); 
	assert (data->precision==1 || data->precision==2 || data->precision==4);

	switch(data->precision) {
		case 1:
		data->intens.i8 = calloc(nIntensities, data->precision);
		break;
		
		case 2:
		data->intens.i16 = calloc(nIntensities, data->precision);
		break;
		
		case 4:
		data->intens.i32 = calloc(nIntensities, data->precision);
		break;
	}
	printf("buffer allocated\n");
	return 0;
}

int freeIntensFromPrecision(INTENS intensities, uint8_t precision) {
	switch(precision) {
		case 1:
		free(intensities.i8);
		break;
		
		case 2:
		free(intensities.i16);
		break;
		
		case 4:
		free(intensities.i32);
		break;
	}
	return 0;
}

int freeIntens(CIFData data)
{
	return freeIntensFromPrecision(data->intens, data->precision);
}

void *getIntensBuffer(CIFData data)
{
	void *buffer;
	switch (data->precision) {
		case 1:
		buffer = (void *) data->intens.i8;
		break;
		
		case 2:
		buffer = (void *) data->intens.i16;
		break;

		case 4:
		buffer = (void *) data->intens.i32;
	}
	return buffer;
}

int setIntensBuffer(CIFData data, void *buffer)
{
	switch (data->precision) {
		case 1:
		data->intens.i8 = (int8_t *) buffer;
		break;

		case 2:
		data->intens.i16 = (int16_t *) buffer;
		break;

		case 4:
		data->intens.i32 = (int32_t *) buffer;
		break;
	}
	return 0;
}

int readcif_data(CIFData data)
{
	FILE * pFile;
	void *buffer = getIntensBuffer(data);
	size_t nIntensities = getIntensSize(data);

	pFile = fopen(data->filename, "rb");
	if (pFile == NULL){
		perror("Error opening file");
		return 1;
	}

	printf("Skipping to intensities\n");
	fseek(pFile, CIF_HEADER_SIZE + 1, SEEK_SET);

	printf("Reading data\n");
	fread(buffer, data->precision, nIntensities, pFile);
	fclose(pFile);

	return 0;
}


