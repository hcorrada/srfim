#include "mex.h"
#include "cifreader.h"
#include <string.h>

mxArray *allocIntensityArray(CIFData data)
{
  mxArray *res;
  mwSize dims[3];

  dims[0]=data->clusterCount;
  dims[1]=NCHANNELS;
  dims[2]=data->nCycles;

  switch(data->precision) {
  case 1:
    res = mxCreateNumericArray(3, dims, mxINT8_CLASS, mxREAL);
    break;

  case 2:
    res = mxCreateNumericArray(3, dims, mxINT16_CLASS, mxREAL);
    break;

  case 4:
    res = mxCreateNumericArray(3, dims, mxINT32_CLASS, mxREAL);
    break;
  }
  return res;
}

mxArray *readit(CIFData data)
{
  int i;
  mxArray *res = NULL;
  size_t buffer_size;
  mwSize dims[3];

  mexPrintf("Precision %d\n", data->precision);
  mexPrintf("cluster count %d\n", data->clusterCount);
  
  if (allocIntens(data)) {
    mexErrMsgTxt("Error allocating intensity buffer");
    free(data);
  }

  if (readcif(data)) {
    mexErrMsgTxt("Error reading data");
    free(data);
  }

  res = allocIntensityArray(data);
  buffer_size = getIntensSize(data) * data->precision;
  
  memcpy(mxGetData(res), (void *) getIntensBuffer(data), buffer_size);
    
  freeIntens(data);
  free(data);
  return res;
}

void mexFunction(int nlhs, mxArray *plhs[], 
		 int nrhs, const mxArray *prhs[])
{
  char *filename;
  int buflen, status, i;
  uint16_t *first_cycle;
  mxArray *intensities;

  if (nlhs == 0 || nlhs > 2) {
    mexErrMsgTxt("One or two outputs required");
  }
  if (nrhs != 1) {
    mexErrMsgTxt("One input required");
  }
  if (mxIsChar(prhs[0]) != 1) {
    mexErrMsgTxt("Input must be a string");
  }

  buflen = (mxGetM(prhs[0]) * mxGetN(prhs[0])) + 1;
  filename = mxCalloc(buflen, sizeof(char));

  status = mxGetString(prhs[0], filename, buflen);
  if (status != 0) {
    mexErrMsgTxt("Error copying filename");
  }
  mexPrintf("Reading file %s\n", filename);

  CIFData data = getCifInfo(filename);
  if (data == NULL) {
    mexErrMsgTxt("Error reading cif file info");
  }

  plhs[0] = readit(data);
  if (nlhs>1) {
    plhs[1] = mxCreateNumericMatrix(1, 1, mxUINT16_CLASS, mxREAL);
    first_cycle = (uint16_t *) mxGetData(plhs[1]);
    first_cycle[0] = data->cycle;
  }
  return;
}
