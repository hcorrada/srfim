#include "cifreader.h"
#include <stdio.h>
#include <stdlib.h>

int test1(const char *filename) {
	int i;

	CIFData data = getCifInfo(filename);
	if (data != NULL) {
		printf("Precision: %d\n", data->precision);
		printf("clusterCount: %u\n", data->clusterCount);
	} else {
		perror("Error reading cif info");
		return 1;
	}

	if (allocIntens(data)) {
		perror("Error allocating intensity buffer");
		free(data);
		return 1;
	}

	readcif_data(data);
	char *buffer = (char *) getIntensBuffer(data);
	for (i=1000; i<1010; i++) {
		printf("%#x ", buffer[i]);
	}
	printf("\n");

	if (getIntensBuffer(data) != NULL) {
		freeIntens(data);
	}
	free(data);


	return 0;
}

int test2(const char *filename) {
	CIFData data;
	char *buffer;
	int i;

	printf("Reading cif file %s\n", filename);
	data = readcif(filename);
	if (data == NULL) {
		perror("Erorr reading ciffile");
		return 1;
	}

	buffer = (char *) getIntensBuffer(data);
	for (i=1000; i<1010; i++) {
		printf("%#x ", buffer[i]);
	}
	printf("\n");

	freeIntens(data);
	free(data);
	return 0;
}
int main(int argc,const char *argv[])
{
	const char *filename = argv[1];
	if(test1(filename)) {
		perror("Test 1 failed");
		return 1; 
	}	

	if (test2(filename)) {
		perror("Test 2 failed");
		return 1;
	}

	return 0;
}