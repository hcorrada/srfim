/*
 * CifReader.c
 *
 *  Created on: Oct 4, 2013
 *      Author: danny
 */


#include <jni.h>
#include <stdio.h>
#include <stdlib.h>
#include <assert.h>
#include "cifreader.h"
#include "CifReader.h"

JNIEXPORT jobject JNICALL Java_CifReader_getCifInfo(JNIEnv *env, jobject thisObj, jstring filename) {

	// Step 1: Convert the JNI String (jstring) into C-String (char*)
	const char *filenameCStr = (*env)->GetStringUTFChars(env, filename, NULL);
	if (NULL == filenameCStr) return NULL;

	// Get a class reference for CifData
	jclass cdClass = (*env)->FindClass(env, "CifData");

	if(cdClass == NULL){
		printf("CifData class has identifier: NULL\n");
	}

	// Get the Method ID of the constructor which takes an int
	jmethodID midInit = (*env)->GetMethodID(env, cdClass, "<init>", "(Ljava/lang/String;BBSSI)V");
	if (NULL == midInit) {
		printf("Did not find constructor");
		return NULL;
	}

	///////////////////////Start Hector's Code///////////////////////////////////
	//printf("Reading cif info from file %s\n", filenameCStr);
	char magic[4] = "\0\0\0\0";
	//open file
	FILE * pFile;

	pFile = fopen(filenameCStr, "rb");
	if (pFile == NULL) {
		perror("Error opening file");
		return NULL;
	}

	fread(magic, 1, 3, pFile);
	if(strncmp(magic, "CIF", 3)) {
		//printf("Got magic string %s", magic);
		fclose(pFile);
		return NULL;
	}

	CIFData data = malloc(sizeof(struct cifData));

	fread(&(data->version), sizeof(data->version), 1, pFile);
	fread(&(data->precision), sizeof(data->precision), 1, pFile);
	fread(&(data->cycle), sizeof(data->cycle), 1, pFile);
	fread(&(data->nCycles), sizeof(data->nCycles), 1, pFile);
	fread(&(data->clusterCount), sizeof(data->clusterCount), 1, pFile);

	//printf("Version: %d\n", data->version); //Changed this line
	//printf("Precision: %d\n",data->precision); //Changed this line
	//printf("cycleRead: %u\n", data->cycle); //Changed this line
	//printf("nCycles: %u\n", data->nCycles); //Changed this line
	//printf("clusterCount %u\n",data->clusterCount); //Changed this line

	fclose(pFile);
	data->filename = filenameCStr;
	/////////////////////////End Hector's Code///////////////////////////////////

	// Call back constructor to allocate a new instance, with an int argument
	jobject newObj = (*env)->NewObject(env, cdClass, midInit, filename, data->version, data->precision, data->cycle, data->nCycles,data->clusterCount);

	(*env)->ReleaseStringUTFChars(env, filename, filenameCStr);  // release resources

	//free(data);
	return newObj;
}

JNIEXPORT jint JNICALL Java_CifReader_getIntensSize(JNIEnv *env, jobject thisObj, jobject cifData){
	//Get a class reference for the cifData object
	jclass cdClass = (*env)->GetObjectClass(env, cifData);
	if(NULL == cdClass){
		printf("Could not acquire class");
		return -1;
	}

	jmethodID getCC = (*env)->GetMethodID(env, cdClass, "getClusterCount", "()I");
	if(NULL == getCC){
		printf("Could not acquire get cluster count");
		return -1;
	}

	jmethodID getNC = (*env)->GetMethodID(env, cdClass, "getnCycles", "()S");
	if(NULL == getNC){
		printf("Could not acquire get number of cycles");
		return -1;
	}

	jshort nCycles = (*env)->CallShortMethod(env, cifData, getNC);
	//printf("In C nCycles is %d\n", (short)nCycles);

	jint clusterCount = (*env)->CallIntMethod(env, cifData, getCC);
	//printf("In C clusterCount is %ld\n", (long)clusterCount);

	jint iSize = NCHANNELS * nCycles * clusterCount;
	//printf("The returned value is: %ld\n",(long)iSize);

	return iSize;
	/*
	 * size_t getIntensSize(CIFData data) {
	 *  size_t nIntensities = NCHANNELS * data->clusterCount * data->nCycles;
	 *  return nIntensities;
	 *  }
	 */
}

JNIEXPORT void JNICALL Java_CifReader_setIntens  (JNIEnv *env, jobject thisObj,
		jstring filename, jbyte precision, jint iSize, jobject intense){
	const char *filenameCStr = (*env)->GetStringUTFChars(env, filename, NULL);
	if (NULL == filenameCStr){
		printf("Could not convert string");
		return;
	}

	// Get a class reference for Intense
	jclass iClass = (*env)->FindClass(env, "Intense");

	if(iClass == NULL){
		printf("Intense class has identifier: NULL\n");
	}

	FILE * pFile;
	pFile = fopen(filenameCStr, "rb");
	if(pFile == NULL){
		perror("Error opening file");
		return;
	}

	//printf("Skipping to intensities\n");
	fseek(pFile, CIF_HEADER_SIZE + 1, SEEK_SET);

	jmethodID setV;
	void *buffer = calloc(iSize, precision);
	int8_t *byteBuff;
	int16_t *shortBuff;
	int32_t *intBuff;

	switch (precision) {
	case 1:
		setV = (*env)->GetMethodID(env, iClass, "setValue", "(IB)V");
		byteBuff = (int8_t *) buffer;
		break;

	case 2:
		setV = (*env)->GetMethodID(env, iClass, "setValue", "(IS)V");
		shortBuff = (int16_t *) buffer;
		break;

	case 4:
		setV = (*env)->GetMethodID(env, iClass, "setValue", "(II)V");
		intBuff = (int32_t *) buffer;
		break;
	}
	fread(buffer, precision, iSize, pFile);

	if(NULL == setV){
		printf("Could not acquire get precision");
		return;
	}

	long c;
	for(c = 0; c < iSize; c++){
		switch (precision) {
		case 1:
			(*env)->CallVoidMethod(env, intense, setV, c, byteBuff[c]);
			break;
		case 2:
			(*env)->CallVoidMethod(env, intense, setV, c, shortBuff[c]);
			break;
		case 4:
			(*env)->CallVoidMethod(env, intense, setV, c, intBuff[c]);
			break;
		}
	}

	fclose(pFile);
	(*env)->ReleaseStringUTFChars(env, filename, filenameCStr);  // release resources

	printf("Completed");
	return;

	/*
	 * int readcif_data(CIFData data)
	 * {
	 * FILE * pFile;
	 * void *buffer = getIntensBuffer(data);
	 * size_t nIntensities = getIntensSize(data);
	 * pFile = fopen(data->filename, "rb");
	 * if (pFile == NULL){
	 * perror("Error opening file");
	 * return 1;
	 * }
	 * printf("Skipping to intensities\n");
	 * fseek(pFile, CIF_HEADER_SIZE + 1, SEEK_SET);
	 * printf("Reading data\n");
	 * fread(buffer, data->precision, nIntensities, pFile);
	 * fclose(pFile);
	 * return 0;
	 * }
	 */
}
