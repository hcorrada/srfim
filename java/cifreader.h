/*
 * cifreader.h
 *
 *  Created on: Oct 4, 2013
 *      Author: Hector Corrado-Bravo
 */


#include <stdint.h>
#include <string.h>

#ifndef _CIFREADER_CIFREADER_H
#define _CIFREADER_CIFREADER_H
#endif

#define NCHANNELS 4
#define CIF_HEADER_SIZE 12

typedef union intens {int8_t * i8; int16_t * i16; int32_t * i32;} INTENS;

struct cifData {
        const char *filename;
        uint8_t version;
        uint8_t precision;
        uint16_t cycle;
        uint16_t nCycles;
        uint32_t clusterCount;
        INTENS intens;
};
typedef struct cifData * CIFData;

CIFData getCifInfo(const char *filename);
size_t getIntensSize(CIFData data);
void *getIntensBuffer(CIFData data);
int setIntensBuffer(CIFData data, void *buffer);
int allocIntens(CIFData data);
int freeIntens(CIFData data);
int freeIntensFromPrecision(INTENS intensities, uint8_t precision);
int readcif_data(CIFData data);

CIFData readcif(const char *filename);
