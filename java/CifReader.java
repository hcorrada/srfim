
public class CifReader {
	static {
		System.loadLibrary("cifjni"); //libcifjni.so
	}
	
	private static CifData cif = null;
	
	private native CifData getCifInfo(String filename);
	
	private native int getIntensSize(CifData data);
	
	private native void setIntens(String filename, byte precision, int iSize, Intense intens);

	public static void main(String args[]) {
		CifReader obj = new CifReader();
		String filename = args[0];
		//String filename = "/host/Users/Danny/Documents/Work/Interships/Hector's Lab/Intensities Project/Data/L006/C1.1/s_6_1.cif";
		
		CifData cif = obj.getCifInfo(filename);
		System.out.println("In Java, the filename is : " + cif.getFilename());
		System.out.println("In Java, the version is : " + cif.getVersion());
		System.out.println("In Java, the precision is : " + cif.getPrecision());
		System.out.println("In Java, the cycle is : " + cif.getCycle());
		System.out.println("In Java, the number of cycles is : " + cif.getnCycles());
		System.out.println("In Java, the clusterCount is : " + cif.getClusterCount());
		
		int iSize = obj.getIntensSize(cif);
		System.out.println("The size for Intens is: " + iSize);
		
		Intense intens = new Intense(cif, iSize);
		//System.out.println("Created intensity object");
		cif.setIntens(intens);
		//System.out.println("Set intensity object");
		obj.setIntens(filename, cif.getPrecision(), iSize, intens);
		
		/*System.out.println("Intensities");
		for(int i = 0; i < iSize; i++){
			System.out.println("Intensity: " + i + " was " + intens.getShortValue(i));
		}
		System.out.println("Intensity: 0A was " + intens.getShortValue(400));
		System.out.println("Intensity: 0C was " + intens.getShortValue(222046+400));
		System.out.println("Intensity: 0G was " + intens.getShortValue(444092+400));
		System.out.println("Intensity: 0T was " + intens.getShortValue(666138+400));
		short[] cOne = cif.getIntensitiesByClusterS(400);
		System.out.println("Got Clusters");
		System.out.println("Intensity: 0A was " + cOne[0]);
		System.out.println("Intensity: 0C was " + cOne[1]);
		System.out.println("Intensity: 0G was " + cOne[2]);
		System.out.println("Intensity: 0T was " + cOne[3]);
		*/
		
		//cif.printAllClusterIntensities();
		short[] request;
		if(args.length < 3){
			request = cif.getIntensitiesByClusterS(Integer.parseInt(args[1]));
			if(request != null){
				System.out.println(request[0] + "\t" + request[1] + "\t" + request[2] + "\t" + request[3]);
			}
		}
		else{
			for(int i = Integer.parseInt(args[1]); i < Integer.parseInt(args[2]); i++){
				request = cif.getIntensitiesByClusterS(i);
				if(request != null){
					System.out.println(request[0] + "\t" + request[1] + "\t" + request[2] + "\t" + request[3]);
				}
			}
		}
	}
}
