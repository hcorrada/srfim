/*
 * Based on the C struct:
 * struct cifData {
 *  const char *filename;
 *  uint8_t version;
 *  uint8_t precision;
 *  uint16_t cycle;
 *  uint16_t nCycles;
 *  uint32_t clusterCount;
 *  INTENS intens;
 *  };
 *  typedef struct cifData * CIFData;
 */

public class CifData {
	private String filename;
	private byte version;
	private byte precision;
	private short cycle;
	private short nCycles;
	private int clusterCount;
	private Intense intens;

	public CifData(String filename, byte version, byte precision, short cycle,
			short nCycles, int clusterCount) {
		this.filename = filename;
		this.version = version;
		this.precision = precision;
		this.cycle = cycle;
		this.nCycles = nCycles;
		this.clusterCount = clusterCount;
		this.intens = null;
	}

	public void setIntens(Intense intens) {
		this.intens = intens;
	}

	public String getFilename() {
		return filename;
	}

	public byte getVersion() {
		return version;
	}

	public byte getPrecision() {
		return precision;
	}

	public short getCycle() {
		return cycle;
	}

	public short getnCycles() {
		return nCycles;
	}

	public int getClusterCount() {
		return clusterCount;
	}

	public Intense getIntens() {
		return intens;
	}

	public byte[] getIntensitiesByClusterB(int cluster){
		if(cluster >= clusterCount){
			System.out.println("The specified cluster is outside of experimental bounds");
			return null;
		}
		else{
			byte a = intens.getByteValue(cluster);
			byte c = intens.getByteValue(cluster + clusterCount);
			byte g = intens.getByteValue(cluster + (2*clusterCount));
			byte t = intens.getByteValue(cluster + (3*clusterCount));
			byte[] result = {a,c,g,t};
			return result;
		}
	}

	public short[] getIntensitiesByClusterS(int cluster){
		if(cluster >= clusterCount){
			System.out.println("The specified cluster is outside of experimental bounds");
			return null;
		}
		else{
			short a = intens.getShortValue(cluster);
			short c = intens.getShortValue(cluster + clusterCount);
			short g = intens.getShortValue(cluster + (2*clusterCount));
			short t = intens.getShortValue(cluster + (3*clusterCount));
			short[] result = {a,c,g,t};
			return result;
		}
	}

	public int[] getIntensitiesByClusterI(int cluster){
		if(cluster >= clusterCount){
			System.out.println("The specified cluster is outside of experimental bounds");
			return null;
		}
		else{
			int a = intens.getIntValue(cluster);
			int c = intens.getIntValue(cluster + clusterCount);
			int g = intens.getIntValue(cluster + (2*clusterCount));
			int t = intens.getIntValue(cluster + (3*clusterCount));
			int[] result = {a,c,g,t};
			return result;
		}
	}
	
	public void printAllIntensities(){
		switch(precision){
		case 1:
			for(int i = 0; i < intens.getSize(); i++){
				System.out.println(intens.getByteValue(i));
			}
			return;
		case 2:
			for(int i = 0; i < intens.getSize(); i++){
				System.out.println(intens.getShortValue(i));
			}
			return;
		case 4:
			for(int i = 0; i < intens.getSize(); i++){
				System.out.println(intens.getIntValue(i));
			}
			return;
		}
	}
	
	public void printAllAIntensities(){
		switch(precision){
		case 1:
			for(int i = 0; i < clusterCount; i++){
				System.out.println(intens.getByteValue(i));
			}
			return;
		case 2:
			for(int i = 0; i < clusterCount; i++){
				System.out.println(intens.getShortValue(i));
			}
			return;
		case 4:
			for(int i = 0; i < clusterCount; i++){
				System.out.println(intens.getIntValue(i));
			}
			return;
		}
		return;
	}
	
	public void printAllCIntensities(){
		switch(precision){
		case 1:
			for(int i = 0; i < clusterCount; i++){
				System.out.println(intens.getByteValue(i + clusterCount));
			}
			return;
		case 2:
			for(int i = 0; i < clusterCount; i++){
				System.out.println(intens.getShortValue(i + clusterCount));
			}
			return;
		case 4:
			for(int i = 0; i < clusterCount; i++){
				System.out.println(intens.getIntValue(i + clusterCount));
			}
			return;
		}
		return;
	}
	
	public void printAllGIntensities(){
		switch(precision){
		case 1:
			for(int i = 0; i < clusterCount; i++){
				System.out.println(intens.getByteValue(i + (2*clusterCount)));
			}
			return;
		case 2:
			for(int i = 0; i < clusterCount; i++){
				System.out.println(intens.getShortValue(i + (2*clusterCount)));
			}
			return;
		case 4:
			for(int i = 0; i < clusterCount; i++){
				System.out.println(intens.getIntValue(i + (2*clusterCount)));
			}
			return;
		}
		return;
	}
	
	public void printAllTIntensities(){
		switch(precision){
		case 1:
			for(int i = 0; i < clusterCount; i++){
				System.out.println(intens.getByteValue(i + (3*clusterCount)));
			}
			return;
		case 2:
			for(int i = 0; i < clusterCount; i++){
				System.out.println(intens.getShortValue(i + (3*clusterCount)));
			}
			return;
		case 4:
			for(int i = 0; i < clusterCount; i++){
				System.out.println(intens.getIntValue(i + (3*clusterCount)));
			}
			return;
		}
		return;
	}
	
	public void printAllClusterIntensities(){
		switch(precision){
		case 1:
			byte[] clusterB;
			for(int i = 0; i < clusterCount; i++){
				clusterB = getIntensitiesByClusterB(i);
				System.out.println("A: " + clusterB[0] + " C: " + clusterB[1] +
						" G: " + clusterB[2] + " T: " + clusterB[3]);
			}
			return;
		case 2:
			short[] clusterS;
			for(int i = 0; i < clusterCount; i++){
				clusterS = getIntensitiesByClusterS(i);
				System.out.println("A: " + clusterS[0] + " C: " + clusterS[1] +
						" G: " + clusterS[2] + " T: " + clusterS[3]);
			}
			return;
		case 4:
			int[] clusterI;
			for(int i = 0; i < clusterCount; i++){
				clusterI = getIntensitiesByClusterI(i);
				System.out.println("A: " + clusterI[0] + " C: " + clusterI[1] +
						" G: " + clusterI[2] + " T: " + clusterI[3]);
			}
			return;
		}
		return;
	}
}
