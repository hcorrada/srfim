/*
 * Based on the C union:
 * typedef union intens {int8_t * i8; int16_t * i16; int32_t * i32;} INTENS;
 */
public class Intense {
	private byte[] i8 = null;
	private short[] i16 = null;
	private int[] i32 = null;
	private final String type;
	private  final int size;
	
	public Intense(CifData cif, int size) {
		this.size = size;
		byte type = cif.getPrecision();
		if(type == 1){
			i8 = new byte[size];
			this.type = "i8";
		}
		else if(type == 2){
			i16 = new short[size];
			this.type = "i16";
		}
		else if(type == 4){
			i32 = new int[size];
			this.type = "i32";
		}
		else{
			this.type = "error";
		}
	}
	
	
	public String getType() {
		return type;
	}


	public int getSize() {
		return size;
	}


	public void setValue(int index, byte value){
		i8[index] = value;
	}
	
	public void setValue(int index, short value){
		i16[index] = value;
	}
	
	public void setValue(int index, int value){
		i32[index] = value;
	}
	
	public byte getByteValue(int index){
		return i8[index];
	}
	
	public short getShortValue(int index){
		return i16[index];
	}
	
	public int getIntValue(int index){
		return i32[index];
	}
}
